﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace poc.netcorebcrypt.Modelos
{
    public class Usuario
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId Id { get; set; }

        [BsonElement("ContrasenaUsuario")]
        public string Contrasena { get; set; }

        [BsonElement("CorreoUsuario")]
        public string Correo { get; set; }


        public Usuario()
        {

        }

        public Usuario(ObjectId id, string contrasena, string correo)
        {
            Id = id;
            Contrasena = contrasena;
            Correo = correo;
        }
    }
}
