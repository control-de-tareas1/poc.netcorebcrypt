﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using poc.netcorebcrypt.Contexto;
using poc.netcorebcrypt.Modelos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace poc.netcorebcrypt.Repositorio
{
    public class UsuarioRepositorio : IUsuarioRepositorio
    {
       // internal MongoDBContexto contexto = new MongoDBContexto("ControlTareas");

        private IMongoCollection<Usuario> Coleccion;
        public UsuarioRepositorio(IMongoDBSettings settings)
        {
            MongoClient cliente  = new MongoClient(settings.Server);
            IMongoDatabase database = cliente.GetDatabase(settings.Database);
            Coleccion = database.GetCollection<Usuario>(settings.UsuarioCollection);
        }
        public async Task<Usuario> Authentication(string mail)
        {
            var filter = Builders<Usuario>.Filter.Where(p => p.Correo == mail);
            return await Coleccion.FindAsync(filter).Result.FirstOrDefaultAsync();
        }

        public Task<List<Usuario>> GetAllUsers()
        {
            throw new NotImplementedException();
        }

        public async  Task InsertUser(Usuario usuario)
        {
            await Coleccion.InsertOneAsync(usuario);
        }
    }
}
