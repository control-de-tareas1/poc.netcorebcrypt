﻿using poc.netcorebcrypt.Modelos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace poc.netcorebcrypt.Repositorio
{
    public interface IUsuarioRepositorio 
    {
        Task<Usuario> Authentication(string mail);
        Task InsertUser(Usuario usuario);
        Task<List<Usuario>> GetAllUsers();
    }
}
