﻿
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using poc.netcorebcrypt.Contexto;
using poc.netcorebcrypt.Helpers;
using poc.netcorebcrypt.Repositorio;
using System;
using System.Configuration;
using System.Windows;



namespace poc.netcorebcrypt
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private readonly IHost host;

        public App()
        {
            host = Host.CreateDefaultBuilder()
                   .ConfigureAppConfiguration((context, builder)=>
                   {
                       builder.AddJsonFile("appsettings.json", optional: true);
                   })
                   .ConfigureServices((context, services) =>
                   {
                       
                       ConfigureServices(context.Configuration, services);
                   })
                   .Build();
        }

        private void ConfigureServices(IConfiguration configuration,
            IServiceCollection services)
        {
            // requires using Microsoft.Extensions.Options

            
            services
                .Configure<MongoDBSettings>(configuration.GetSection(nameof(MongoDBSettings)))
                .AddSingleton<IMongoDBSettings>(sp => sp.GetRequiredService<IOptions<MongoDBSettings>>().Value)
                .AddSingleton<UsuarioRepositorio>()
                .AddSingleton<ILoginHelper, LoginHelper>()
                .AddScoped<LoginBcrypt>();

        }

        protected override async void OnStartup(StartupEventArgs e)
        {
            await host.StartAsync();

            var mainWindow = host.Services.GetService<LoginBcrypt>();
            mainWindow.Show();
            base.OnStartup(e);
        }

        protected override async void OnExit(ExitEventArgs e)
        {
            using (host)
            {
                await host.StopAsync(TimeSpan.FromSeconds(5));
            }

            base.OnExit(e);
        }

    }
}
