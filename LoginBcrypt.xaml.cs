﻿using ControlzEx.Theming;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using poc.netcorebcrypt.Helpers;
using poc.netcorebcrypt.Modelos;
using poc.netcorebcrypt.Repositorio;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace poc.netcorebcrypt
{
    /// <summary>
    /// Lógica de interacción para LoginBcrypt.xaml
    /// </summary>
    public partial class LoginBcrypt : MetroWindow
    {
        private Usuario usuario;
        private Usuario res;
        public readonly UsuarioRepositorio usuarioRepositorio;
        private readonly ILoginHelper loginHelper;
        public delegate void UpdateProgressFunc(double value);



        public LoginBcrypt(UsuarioRepositorio usuarioRepositorio, ILoginHelper loginHelper)
        {
            InitializeComponent();
            this.usuarioRepositorio = usuarioRepositorio;
            this.loginHelper = loginHelper;
            ColorsCombo.ItemsSource = ThemeManager.Current.ColorSchemes;
            ThemeCombo.ItemsSource = ThemeManager.Current.BaseColors;

        }

        private async Task Autenticar(ProgressDialogController controller)
        {


            if (loginHelper.EmptyTextBox(txtCorreo.Text, txtPassword.Text))
            {

                res = await usuarioRepositorio.Authentication(txtCorreo.Text);
                if (res != null)
                {
                    bool isValidPassword = loginHelper.ValidatePassword(txtPassword.Text, res.Contrasena);
                    if (isValidPassword)
                    {
                        usuario = res;

                    }
                    else
                    {
                        await controller.CloseAsync();
                        await BasicDialog($"Campos Incorrectos", $"Contraseña Invalida");
                    }
                }
                else
                {
                    await controller.CloseAsync();
                    await BasicDialog($"Campos Incorrectos", $"Correo no existe");
                }

                if (usuario != null)
                {

                    await controller.CloseAsync();
                    await BasicDialog($"Bienvenido a Troya", $"Planifica y Organiza tu empresa");
                    progressBarLogin.IsIndeterminate = false;

                    MainWindow mainWindow = new MainWindow();
                    mainWindow.Show();

                    var parentWindow = Window.GetWindow(this);
                    parentWindow.Close();
                }

            }
            else
            {
                await controller.CloseAsync();
                await BasicDialog($"Campos Incorrectos", $"Campos Vacíos");

            }


        }

        private async void ingresarButton_Click(object sender, RoutedEventArgs e)
        {

            var controller = await this.ShowProgressAsync("Por favor espere", "Iniciando Sesión...");

            //progressBarLogin.IsIndeterminate = true;
            await Task.Delay(2000);
            await Dispatcher.Invoke(async () =>
            {

                await Autenticar(controller);

            });

            //progressBarLogin.IsIndeterminate = false;


        }

        public async Task BasicDialog(string Text1, string Text2)
        {

            MetroDialogSettings Miconf = new MetroDialogSettings()
            {
                AffirmativeButtonText = "OK",
                ColorScheme = MetroDialogColorScheme.Accented
            };

            await this.ShowMessageAsync(Text1, Text2, MessageDialogStyle.Affirmative,
                Miconf);
        }


        #region Alto cotraste

        private void ColorsCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            var selectedValue = ColorsCombo.SelectedItem as string;
            if (selectedValue == "Blue")
                ThemeManager.Current.ChangeThemeColorScheme(App.Current, selectedValue);
            if (selectedValue == "Red")
                ThemeManager.Current.ChangeThemeColorScheme(App.Current, selectedValue);
            if (selectedValue == "Green")
                ThemeManager.Current.ChangeThemeColorScheme(App.Current, selectedValue);
            if (selectedValue == "Purple")
                ThemeManager.Current.ChangeThemeColorScheme(App.Current, selectedValue);
            if (selectedValue == "Orange")
                ThemeManager.Current.ChangeThemeColorScheme(App.Current, selectedValue);
            if (selectedValue == "Lime")
                ThemeManager.Current.ChangeThemeColorScheme(App.Current, selectedValue);
            if (selectedValue == "Emerald")
                ThemeManager.Current.ChangeThemeColorScheme(App.Current, selectedValue);
            if (selectedValue == "Teal")
                ThemeManager.Current.ChangeThemeColorScheme(App.Current, selectedValue);
            if (selectedValue == "Cyan")
                ThemeManager.Current.ChangeThemeColorScheme(App.Current, selectedValue);
            if (selectedValue == "Cobalt")
                ThemeManager.Current.ChangeThemeColorScheme(App.Current, selectedValue);
            if (selectedValue == "Indigo")
                ThemeManager.Current.ChangeThemeColorScheme(App.Current, selectedValue);
            if (selectedValue == "Violet")
                ThemeManager.Current.ChangeThemeColorScheme(App.Current, selectedValue);
            if (selectedValue == "Pink")
                ThemeManager.Current.ChangeThemeColorScheme(App.Current, selectedValue);
            if (selectedValue == "Magenta")
                ThemeManager.Current.ChangeThemeColorScheme(App.Current, selectedValue);
            if (selectedValue == "Crimson")
                ThemeManager.Current.ChangeThemeColorScheme(App.Current, selectedValue);
            if (selectedValue == "Amber")
                ThemeManager.Current.ChangeThemeColorScheme(App.Current, selectedValue);
            if (selectedValue == "Yellow")
                ThemeManager.Current.ChangeThemeColorScheme(App.Current, selectedValue);
            if (selectedValue == "Brown")
                ThemeManager.Current.ChangeThemeColorScheme(App.Current, selectedValue);
            if (selectedValue == "Olive")
                ThemeManager.Current.ChangeThemeColorScheme(App.Current, selectedValue);
            if (selectedValue == "Steel")
                ThemeManager.Current.ChangeThemeColorScheme(App.Current, selectedValue);
            if (selectedValue == "Mauve")
                ThemeManager.Current.ChangeThemeColorScheme(App.Current, selectedValue);
            if (selectedValue == "Taupe")
                ThemeManager.Current.ChangeThemeColorScheme(App.Current, selectedValue);
            if (selectedValue == "Sienna")
                ThemeManager.Current.ChangeThemeColorScheme(App.Current, selectedValue);

        }

        private void ThemeCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var Light = ThemeManager.BaseColorLightConst;
            var Dark = ThemeManager.BaseColorDarkConst;
            var selectedValue = ThemeCombo.SelectedItem as string;
            if (selectedValue == "Dark")
                ThemeManager.Current.ChangeThemeBaseColor(App.Current, Dark);
            if (selectedValue == "Light")
                ThemeManager.Current.ChangeThemeBaseColor(App.Current, Light);
        }
        #endregion


        private async void  TestButton_Click(object sender, RoutedEventArgs e)
        {
            var settings = new MetroDialogSettings
            {
                NegativeButtonText = "Cancelar",
                AnimateShow = true
               
            }; 
            var controller = await this.ShowProgressAsync("Por favor espere", "Iniciando Sesión..." , true, settings);
            controller.SetProgress(0);
            await Task.Delay(1000);
            controller.SetProgress(0.1);
            if (controller.IsCanceled)
            {
                await Task.Delay(1000);
                controller.SetProgress(1);
                await controller.CloseAsync();
                await this.ShowMessageAsync("???????????", "??????");
                return;
            }
            await Task.Delay(1000);
            controller.SetProgress(0.3);
            controller.SetMessage("Cargando...");
            await Task.Delay(500);
            controller.SetProgress(0.7);


            var result = await ValidUser();
            await Task.Delay(900);
            controller.SetProgress(1);
            await Task.Delay(900);

            await controller.CloseAsync();
        }


        private async Task<Usuario>  ValidUser()
        {
            return await usuarioRepositorio.Authentication(txtCorreo.Text); 
         }



    }


    
}
