﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace poc.netcorebcrypt.Helpers
{
    public class LoginHelper : ILoginHelper
    {
        public bool EmptyTextBox(string txtcorreo, string txtpassword)
        {
            if (txtcorreo != string.Empty && txtpassword != string.Empty)
            {
                return true;
            }

            return false;
        }

        public string HashPassword(string passsword )
        {
           return BCrypt.Net.BCrypt.HashPassword(passsword);
        }

        public bool ValidatePassword(string NormalPassword, string HashingPassword)
        {
            bool isValidPassword = BCrypt.Net.BCrypt.Verify(NormalPassword, HashingPassword);
            if (isValidPassword)
            {
                return true;
            }

            return false;
        }
    }
}
