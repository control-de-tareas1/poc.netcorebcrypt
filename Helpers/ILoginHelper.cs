﻿using System;
using System.Collections.Generic;
using System.Text;

namespace poc.netcorebcrypt.Helpers
{
    public interface ILoginHelper
    {
        bool EmptyTextBox(string txtcorreo, string txtpassword);
        string HashPassword(string passsword);
        bool ValidatePassword(string NormalPassword, string HashingPassword);


    }
}
