﻿using System;
using System.Collections.Generic;
using System.Text;

namespace poc.netcorebcrypt.Contexto
{
    public interface IMongoDBSettings
    {
        string Server { get; set; }

        string Database { get; set; }

        string UsuarioCollection { get; set; }
    }
}
