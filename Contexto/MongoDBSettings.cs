﻿using System;
using System.Collections.Generic;
using System.Text;

namespace poc.netcorebcrypt.Contexto
{
    public class MongoDBSettings : IMongoDBSettings
    {
        public string Server { get; set; }
        public string Database { get; set; }
        public string UsuarioCollection { get; set; }

    }
}
